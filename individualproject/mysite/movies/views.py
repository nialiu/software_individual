from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, CreateView, UpdateView, TemplateView
from django.urls import reverse_lazy
#from django.template import loader
from .models import Menu

#def index(request):
#    return HttpResponse("Hello world!  You're at the movies index.")

def index(request):
    list_of_movies = Menu.objects.all().order_by('name')
#	form = request.post
#    template = loader.get_template('movies/menu_list.html')
    context = {
        'list_of_movies': list_of_movies,
    }
    return render(request, 'movies/menu_list.html', context)
#	return HttpResponse(template.render(context, request))

class MenuListView(ListView):
    model = Menu
    context_object_name = 'movies'

#class MenuCreateView(CreateView):
#    model = Menu
#    fields = ('id', 'name', 'starts', 'ends', 'genre', 'rating', 'room', 'synopsis')
#    success_url = reverse_lazy('menu_changelist')

#class MenuUpdateView(UpdateView):
#    model = Menu
#    fields = ('id', 'name', 'starts', 'ends', 'genre', 'rating', 'room', 'synopsis')
#    success_url = reverse_lazy('menu_changelist')

#class MenuPageView(TemplateView):
#    template = 'menu_list.html'
