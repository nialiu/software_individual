from django.db import models

class Menu(models.Model):
    name = models.CharField(max_length=30)
    
    def __str__(self):
        return self.name
		
class Movie(models.Model):
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    
    def __str__(self):
        return self.name
